<?php
declare(strict_types=1);
namespace App\Forms;

use Nette\Application\UI\Form;
use Nette\SmartObject;

class TranslateFormFactory
{
    use SmartObject;

    private FormFactory $formFactory;

    public function __construct(FormFactory $formFactory)
    {
        $this->formFactory=$formFactory;
    }

    public function create(callable $onSuccess): Form
    {
       $form=$this->formFactory->create();
        $form->addText('translate', 'Text k překladu:')->setRequired()
            ->addRule($form::MIN_LENGTH, 'Slovo je příliš krátké', 2);;
        $form->addSubmit('send', 'Odeslat');
        $form->onSuccess[] = function($form,$values) use ($onSuccess) {
            $onSuccess($form, $values);
        };
        return $form;
    }
}

