<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Model\TranslateManager;
use Nette\Application\UI\Form;
use App\Forms\TranslateFormFactory;

final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    private TranslateManager $translateManager;
    private TranslateFormFactory $translateFormFactory;
    private ?string $displayTranslateWord;
    private bool $error;
    public function __construct(TranslateManager $translateManager,TranslateFormFactory $translateFormFactory)
    {
        parent::__construct();
        $this->translateManager=$translateManager;
        $this->displayTranslateWord="";
        $this->error=false;
        $this->translateFormFactory=$translateFormFactory;
    }

    protected function createComponentTranslateForm(): Form
    {
        return $this->translateFormFactory->create(function ($form,$values){
         $this->formSucceeded($form,$values);
    });
    }
    public function renderDefault():void
    {
        $this->template->displayTranslateWord=$this->displayTranslateWord;
        $this->template->error=$this->error;
    }
    public function formSucceeded(Form $form, Nette\Utils\ArrayHash $data): void
    {
        $translatedWord=$this->translateManager->translateWord($data->translate);
        if(!$translatedWord)
            $this->error=true;
       $this->displayTranslateWord=$translatedWord;
    }

}
