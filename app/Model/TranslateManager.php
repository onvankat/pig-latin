<?php
namespace App\Model;

use Nette;

final class TranslateManager
{
    use Nette\SmartObject;
    const VOWELS="aeiou",
          CONSONANT="bcdfghjklmnpqrstvwxyz",
            SUFFIX="ay";

    private array $vowelsArray;
    private array $consonantArray;
    private array $translatedWordArray;

    public function __construct()
    {
        $this->vowelsArray=$this->toArray(self::VOWELS);
        $this->consonantArray=$this->toArray(self::CONSONANT);
    }

    /**
     * Zapíše slovo k překladu do atributu vrátí výsledné slovo z funkce editWord
     * @param string $translateWord slovo k překladu
     * @return string
     */
    public function translateWord(string $translateWord):?string
    {

        $this->translatedWordArray=$this->toArray($translateWord);
        return $this->editWord();

    }

    /**
     * Převede string v parametru na malá písmena a převede na pole dle písmen
     * @param string $text slovo k převodu
     * @return array
     */
    public function toArray(string $text):array
    {
        $text=strtolower($text);
        return str_split($text);
    }

    /**
     * Upraví slovo do finalní podoby dle funkce checkWord, vrací null v případě chyby
     * @return ?string
     */
    private function editWord():?string
    {
        $information=$this->checkWord();
        if(empty($information))
        return null;
        if($information['type']=='vowel' && $information['letterNumber']==1)
        {
            return $this->addSuffix();
        }
        elseif($information['type']=='vowel')
        {
            $wordWithMovedCons=$this->transformConsonant($information['letterNumber']-1);
            return $this->addSuffix($wordWithMovedCons);
        }
        else
        {
            $wordWithMovedCons=$this->transformConsonant(2);
            return $this->addSuffix($wordWithMovedCons);
        }
    }

    /**
     * Projde písmena slova k překladu a zkoumá přítomnost, samohlásek, souhlásek a počet písmen..prázdné pole v případě chyby
     * @return array
     */
    private function checkWord() :array
    {
        $information=array();
        $i=1;
        $numberOfLetters=count($this->translatedWordArray);
        foreach($this->translatedWordArray as $word)
        {
            if(in_array($word,$this->vowelsArray))
            {
                $information['type']="vowel";
                $information['letterNumber']=$i;
                    return $information;
            }
            elseif (in_array($word,$this->consonantArray)){
                $information['type']="consonant";
                $information['letterNumber']=$i;
                    if($numberOfLetters==$i) {
                     return $information;
                     }
            }
            else
            {
                $information=array();
                return $information;
            }

            $i++;
        }
        return $information;
    }

    /**
     * Odstraňuje souhlásky ze začátku slova a přesouvá je
     * @param int $lettersToMove počet souhlásek ze začátku slova, k odstranění a přesunu
     * @return array
     */
    private function transformConsonant(int $lettersToMove):array
    {
        $arrayWithSpicedLetters=array();
        for($i=0;$i<$lettersToMove;$i++)
        {
            $arrayWithSpicedLetters[]=$this->translatedWordArray[$i];
        }
        return array_slice(array_merge($this->translatedWordArray,$arrayWithSpicedLetters),$lettersToMove);
    }

    /**
     * Přidává příponu na konec slova
     * @param ?array $wordArray Přidá příponu ke slovu v parametru, pokud je prázdný použije atribut
     * @return string
     */
    private function addSuffix(array $wordArray=null) :string
    {
        if(!$wordArray)
            $wordArray=$this->translatedWordArray;
        return implode("",$wordArray).self::SUFFIX;
    }

}